console.log(`------------------------------------------------------------------------`);
console.log(`                        Proxmox Terminal Proxy                          `);
console.log(`     Created by Jay W (https://gitlab.com/codingJWilliams/termproxy)    `);
console.log(`------------------------------------------------------------------------`);


import * as WebSocket from 'ws';
import Axios from "axios";
import * as fs from "fs";
import Logger from 'dectom-console-logger';
import {Agent} from "https";
import * as jsonwebtoken from 'jsonwebtoken';

const config = JSON.parse(fs.readFileSync("./config.json", "utf-8"));
const wss = new WebSocket.Server({ port: config.port });
const instance = Axios.create({
  baseURL: config.proxmoxUrl,
  httpsAgent: new Agent({ rejectUnauthorized: false })
});
if (config.tlsUnauthorized) process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

interface ProxmoxTicket {
  csrf: string,
  ticket: string
}
interface VNCTicket {
  port: string,
  user: string,
  ticket: string
}
interface TerminalRequest {
  dateRequested: Date,
  vmid: string
}

let currentProxmoxTicket:{ ticket: ProxmoxTicket, obtained: Date } = {ticket: null, obtained: null};

wss.on('connection', async function connection(ws: WebSocket, req: Request) {
  Logger.info("New connection");

  // Parse encoded JSONWebToken
  let jwt: string = req.url.replace("/?jwt=", "");
  let request: TerminalRequest;

  try { 
    request = jsonwebtoken.verify(jwt, config.tokenPublicKey) as TerminalRequest;
    Logger.info(`JSONWebToken verified successfully - Data: ${request}`);
  }
  catch (err) {
    if (err == jsonwebtoken.JsonWebTokenError) {
      Logger.error("Disconnected client due to JSON web token error");
      ws.send("ERR_INVALID_TOKEN");
      ws.terminate()
    }
  }

  // If we don't have a generic ticket, or if the current ticket is older than 1 hour
  if (!currentProxmoxTicket.ticket || (currentProxmoxTicket.obtained.getTime() - Date.now()) > 1000 * 60 * 60) {
    Logger.info("Refreshing auth ticket...")
    currentProxmoxTicket.ticket = (await instance.post("/api2/json/access/ticket", `username=${config.username}&password=${config.password}`)).data.data;
  }
  Logger.info("Generic auth ticket is available and not expired")
  // Get a special VNC ticket
  let vncTicket: VNCTicket = (await instance.post(`/api2/json/nodes/voidcrafted/lxc/${request.vmid}/termproxy`, "", {
    headers: {
      CSRFPreventionToken: currentProxmoxTicket.ticket.csrf,
      Cookie: "PVEAuthCookie=" + encodeURIComponent(currentProxmoxTicket.ticket.ticket)
    }
  })).data.data;

  // Open connection to upstream proxmox
  const wsProxmox = new WebSocket(`${config.proxmoxWsUrl}/api2/json/nodes/voidcrafted/lxc/${request.vmid}/vncwebsocket?port=${vncTicket.port}&vncticket=${encodeURIComponent(vncTicket.ticket)}`, 'binary', {
    headers: {
      CSRFPreventionToken: currentProxmoxTicket.ticket.csrf,
      Cookie: "PVEAuthCookie=" + encodeURIComponent(currentProxmoxTicket.ticket.ticket)
    }
  });
  wsProxmox.binaryType = 'arraybuffer';
  wsProxmox.on("open", () => {
    Logger.info("Upstream proxmox connection opened, sending auth...");
    wsProxmox.send(vncTicket.user + ":" + vncTicket.ticket + "\n");
    Logger.info("Sending downstream client the ready message");
    ws.send("ready");
  })

  // Proxy data back and forth
  wsProxmox.on("message", (m) => {
    ws.send(m)
  })
  ws.on('message', (m) => {
    wsProxmox.send(m);
  });

  // Proxy closes
  wsProxmox.on("close", () => {
    Logger.info("Connection closed by upstream");
    ws.close()
  });
  ws.on("close", () => {
    Logger.info("Connection closed by downstream");
    wsProxmox.close()
  });
});