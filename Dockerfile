FROM node:12-alpine
WORKDIR /app/build/
COPY package.json .
COPY package-lock.json .
RUN npm install --dev
COPY *.ts .
RUN tsc

FROM node:12-alpine
WORKDIR /app/run
COPY --from=0 /app/build/index.js .
COPY --from=0 /app/build/node_modules .
COPY config.json .
RUN npm install
CMD ["node", "./index.js"]  